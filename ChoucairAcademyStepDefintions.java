package co.com.choucair.cerfication.proyectobase.stepdefinitions;

import co.com.choucair.certification.proyectobase.tasks.Login;
import co.com.choucair.certification.proyectobase.tasks.OpenUp;
import co.com.choucair.certification.proyectobase.tasks.Search;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class ChoucairAcademyStepDefintions {


    private String course;

    @Before
    public void setStage () {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^than brandon wats to learn automation at the academy choucair$")
    public void ThanBrandonWatsToLearnAutomationAtTheAcademyChoucair() throws Exception {
        //write code here that turns the phrase above into concrete actions
        OnStage.theActorCalled( "Brandon").wasAbleTo(OpenUp.thePage(), (Login.OnThePage()));
        //throw new PendingException();
    }

    @When("he search for the course (.*) on the choucair academy platform$")
    public void HeSearchForTheCourseRecursosAutomatizacionBancolombiaOnTheChoucairAcademyPlatforms(String Course) throws Exception{
        //write code here that turns the phrase above into concrete action
        OnStage.theActorInTheSpotlight().attemptsTo(Search.the(course));
        //throw new PendingException();
    }

    @Then("he finds the course called resources Recursos Automatizacion Bancolombia$")
    public void HeFindsTheCourseCalledResourcesRecursosAutomatizacionBancolombia() throws Exception {
        //write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }
}
