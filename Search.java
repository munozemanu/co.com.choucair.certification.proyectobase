package co.com.choucair.certification.proyectobase.tasks;

import co.com.choucair.certification.proyectobase.userinterface.SearchCoursePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;

public class Search implements Task {
    public static Search the(String course) {return Tasks.instrumented(Search.class);}

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SearchCoursePage.BUTTON_UC),
                Enter.theValue("Buscar Curso").into(SearchCoursePage.INPUT_COURSE),
                Enter.theValue("Dar Click para Buscar").into(SearchCoursePage.BUTTON_GO),
                Enter.theValue("Selecionar curso").into(SearchCoursePage.SELECT_COURSE)
        );
    }
}
